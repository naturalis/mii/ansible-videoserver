# ansible-videoserver

Ansible role that installs a videoserver to store recorded stream on over SMB, which nightly pushes those to Google Drive with RClone

## Requirements

This role is tested on Ubuntu 18.04 (server edition), so you will need to have a
machine running that OS. You need to at least use ansible version 2.7 or higher.
On the destination machine python has to be installed.

## Role Variables

The relevant variables for this role are:

* `samba_user` with subvariables `name`, `id` and `home`. This user will be used
   by the client to login to the share.
* `samba_user_password`. Please change the default password (and use Ansible vault to
   encrypt the secret).
* `rclone_drive_location`. Location on Google Drive to store the video files.
* `conn_name`. Name of the extra 10GbE NIC.
* `ip4`. IP address of the extra 10GbE NIC.

For example:

   ```text
   samba_user:
     name: videouser
     id: 1112
     home: /home/videouser

   samba_user_password: !vault |
             $ANSIBLE_VAULT;1.1;AES256
             62616363633634616133646331343134316436633832663235353730303633663732386335323665
             3137336430626665313337363638303936376133636361310a333839623431313463646333666235
             33323463343966633066323166336138343238373436386133323664336138646138613361633830
             3863656463616364640a376430366337653434313831393864353261326566396634383961336338
             6166
   rclone_drive_location: Backup_test
   conn_name: enp6s0
   ip4: 172.16.66.3/24
   ```
